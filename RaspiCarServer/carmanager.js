

var idRange=1000;

function createManager() {
	function Car(name, socket) {
		this.name=name;
		this.free=true;
		//Keep a reference on the socket so we can publish data later
		this.socket=socket;
		this.id=Math.floor(Math.random()*1000);
	}
	
	var carNames= [];
	var cars = new Array(carNames.length);
	var i;
	for(i=0; i<cars.length; i++) {
		cars[i] = new Car(carNames[i]);
	}
	
	function ensureNoIdDuplicate() {
		for(i=0;i<cars.length;i++) {
			console.log(cars[i]);	
			for(var j=i+1; j<cars.length; j++) {	
				if(cars[i].id==cars[j].id) {
					cars[j].id=Math.floor(Math.random()*1000);
					ensureNoIdDuplicate();
					return;
				}
			}
		}
	}
	ensureNoIdDuplicate();
	
	this.findCarWithId= function(id) {
		for(i=0; i<cars.length;i++) {
			if(cars[i].id == id) {
				return cars[i];
			}
		}
		return undefined;
	}
	
	this.findFreeCar = function() {
		for(i=0; i<cars.length; i++) {
			if(cars[i].free) {
				cars[i].free=false;
				return cars[i];
			}
		}
	}
	
	this.getFreeCarsId = function() {
		var freecars=[];
		for(i=0; i<cars.length;i++) {
			if(cars[i].free) {
				freecars.push(cars[i].id);
			}
		}
		
		return freecars;
	}
	
	this.addCar = function(name, socket) {
		var car = new Car(name,socket);
		cars.push(car);
		ensureNoIdDuplicate();
		return car.id;
	}
	
	this.removeCar = function(socket) {
		for(i=0; i<cars.length;i++) {
			if(cars[i].socket) {
				if(cars[i].socket==socket) {
					cars.remove(cars[i]);
					return;
				}
			}
		}
	}

	this.areFreeCarsThere = function() {
		cars.forEach(function(value){
			console.log(value.free);
			if(value.free) return true;
		})
		return false;
	}

	this.getAllCars = function() {
		var simplifiedCars = [];
		cars.forEach(function (value) {
			simplifiedCars.push(simplifyCar(value))
		})
		return simplifiedCars;
	}

	this.getSimpleCarById = function(id) {
		var ret;
		cars.forEach(function(value){
			console.log(id+"   "+value.id);
			if(value.id==id) {
				ret = simplifyCar(value);
				
			}
		})
		return ret;
	}

	this.simplifyCar = function(car) {
		return {
			name:car.name,
			id:car.id,
			free:car.free
		}
	}

	

	
	return this;
	
	
	
	
	
}

module.exports= createManager();
	
	

