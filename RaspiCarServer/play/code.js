


function initialize(){
	
	document.getElementById("check").onclick=checkboxChanged;
	var canvas = document.getElementById("canvas");
	var BORDER_SIZE = 5;
	var INDICICATOR_SIZE = 10;
	var ctx = canvas.getContext("2d");
	
	var accelerationInfo = document.getElementById("acceleration");
	var angleInfo = document.getElementById("angle");
	ctx.fillStyle="red";
	ctx.fillRect(0,0,canvas.width,canvas.height);
	ctx.clearRect(BORDER_SIZE,BORDER_SIZE, canvas.width-BORDER_SIZE*2, canvas.height-BORDER_SIZE*2)
	/*var gyroinfo = document.getElementById("gyroinfo");
	var gyrofound=false;
	if (window.DeviceMotionEvent == undefined) {
    	gyroinfo.innerHTML="No gyrometer found.."
    }
    else {
		gyrofound=true;
    	gyroinfo.innerHTML="Gyrometer found!"
        //window.addEventListener("devicemotion", accelerometerUpdate, true);
    }
	*/
	//canvas.addEventListener("click", handleClick, true);

	window.addEventListener("mousedown", mouseDown);
	window.addEventListener("mouseup", mouseUp);
	canvas.addEventListener("mousemove", onMouseMove, true);
	window.addEventListener("touchmove", onTouch, true);
	
	window.addEventListener("touchmove", function(data){
		//console.log("te");
	},true)
	
	function onTouch(event) {
		var touchList = event.touches;
		for(var i=0; i<touchList.length;i++) {
			var item = touchList.item(i);
			var hitX = parseInt(item.pageX);
			var hitY = parseInt(item.pageY);
			var xPos = hitX-canvas.offsetLeft
			var yPos = hitY-canvas.offsetTop
			if(xPos<canvas.width && yPos<canvas.height) {
				ctx.clearRect(BORDER_SIZE,BORDER_SIZE, canvas.width-BORDER_SIZE*2, canvas.height-BORDER_SIZE*2)
				console.log(xPos+" "+yPos);
				ctx.fillRect(xPos-INDICICATOR_SIZE/2, yPos-INDICICATOR_SIZE/2, INDICICATOR_SIZE,INDICICATOR_SIZE);
				console.log(xPos+"   "+yPos);
		
				var angle =  (-canvas.width/2+xPos) / (canvas.width/2); 
		
				if(angle>0.25) {
					angle=0.5
				} else if(angle<-0.25) {
					angle = -0.5;
				} else {
					angle=0;
				}
				var acceleration = (canvas.height/2-yPos) / (canvas.height/2);
				console.log("angle is: "+angle);
				console.log("acceleration is: "+acceleration)
				angleInfo.innerHTML="angle is: "+angle;
				accelerationInfo.innerHTML="acceleration is: "+acceleration;
				socket.emit("acceleration", acceleration);
				socket.emit("angle", angle);
			}
		}
	}
	
	//window.addEventListener("deviceorientation", handleOrientation, true);
	//if(gyrofound)
	socket = io.connect("/raspicar/client");
	socket.on("connect", function(data) {
		var current = window.location.href;
		var index = current.lastIndexOf("/");
		current= current.substring(0, index);
		index = current.lastIndexOf("/");
		current= current.substring(index+1, current.length);
		socket.emit("register", current );
	})
	socket.on("car", function(data){
		document.getElementById("carinfo").innerHTML="Your car has the name: "+data;
	});

	socket.on("raspi/rstop", function(data){
		console.log("race finished");
	})
	socket.on("info", function(data){
		document.getElementById("carinfo").innerHTML=data;
	})
	
	function onRaceEnd() {
		//TODO: Make yes no alert here
		alert("Das Rennen wurde beendet, willst du auf die Startseite weitergeleitet werden?")
	}
	function accelerometerUpdate(e) {
		
		ctx.clearRect(0,0, canvas.width, canvas.height);
		aX = event.accelerationIncludingGravity.x;
		aY = event.accelerationIncludingGravity.y;
		aZ = event.accelerationIncludingGravity.z;
		ctx.fillStyle="#FF0000";
		ctx.fillRect(100-aX*10, 100+aY*10, 10,10);
		
		xPosition = Math.atan2(aY, aZ);
		yPosition = Math.atan2(aX, aZ);
		//socket.emit('x', xPosition);
		//socket.emit('y', yPosition);
	}
	
	function handleOrientation(event) {
		
		//event.beta seems to be the correct value. it 
		
		var angle = event.beta;
		if(angle>-45 && angle<45) {
			socket.emit("angle", -angle/45);
		}
	
		var acceleration = event.gamma;
		var abs = Math.abs(acceleration);
		
		if(abs>45 && abs<90) {
			if(acceleration>0) {
				
				socket.emit("acceleration", (90-acceleration)/45);
			} else {
				socket.emit("acceleration", (-90-acceleration)/45);
			}
			
		}

		//if(event.alpha>180) value=event.alpha-180;
		//else value=event.alpha;
		
		
	}
	
	
	var isMouseDown=false;
	function mouseDown() {
		isMouseDown=true;
	}
	
	function mouseUp() {
	
		isMouseDown=false;
	}
	
	function onMouseMove(event) {
		console.log("mousemove")
		if(!isMouseDown) return;
		ctx.clearRect(BORDER_SIZE,BORDER_SIZE, canvas.width-BORDER_SIZE*2, canvas.height-BORDER_SIZE*2)
		var xPos = event.pageX-canvas.offsetLeft
		var yPos = event.pageY-canvas.offsetTop
		ctx.fillRect(xPos-INDICICATOR_SIZE/2, yPos-INDICICATOR_SIZE/2, INDICICATOR_SIZE,INDICICATOR_SIZE);
		console.log(xPos+"   "+yPos);
		
		var angle =  (-canvas.width/2+xPos) / (canvas.width/2); 
		
		if(angle>0.25) {
			angle=0.5
		} else if(angle<-0.25) {
			angle = -0.5;
		} else {
			angle=0;
		}
		var acceleration = (canvas.height/2-yPos) / (canvas.height/2);
		console.log("angle is: "+angle);
		console.log("acceleration is: "+acceleration)
		angleInfo.innerHTML="angle is: "+angle;
		accelerationInfo.innerHTML="acceleration is: "+acceleration;
		socket.emit("acceleration", acceleration);
		socket.emit("angle", angle);
				
	}
	
	
	
	

	
	function handleClick(event) {
		ctx.clearRect(BORDER_SIZE,BORDER_SIZE, canvas.width-BORDER_SIZE*2, canvas.height-BORDER_SIZE*2)
		var xPos = event.pageX-canvas.offsetLeft
		var yPos = event.pageY-canvas.offsetTop
		ctx.fillRect(xPos-INDICICATOR_SIZE/2, yPos-INDICICATOR_SIZE/2, INDICICATOR_SIZE,INDICICATOR_SIZE);
		console.log(xPos+"   "+yPos);
		
		var angle =  (-canvas.width/2+xPos) / (canvas.width/2); 
		var acceleration = (canvas.height/2-yPos) / (canvas.height/2);
		console.log("angle is: "+angle);
		console.log("acceleration is: "+acceleration)
		angleInfo.innerHTML="angle is: "+angle;
		accelerationInfo.innerHTML="acceleration is: "+acceleration;
		
		socket.emit("acceleration", acceleration)
		socket.emit("angle", angle);
	}
	
	var useGyros = false;
	
	function checkboxChanged() {
		
		if(useGyros) {
		//	window.removeEventListener("deviceorientation", handleOrientation, true);
			canvas.addEventListener("click", handleClick, true);
			canvas.addEventListener("mousemove", onMouseMove, true);
			
		} else {
		//	window.addEventListener("deviceorientation", handleOrientation, true);
			canvas.removeEventListener("click", handleClick, true);
			canvas.removeEventListener("mousemove", onMouseMove, true);
		}
		
		useGyros = !useGyros;
	}

	

	
}



function accelerateClicked() {
	socket.emit("acceleration", 1)
}

function stopClicked() {
	socket.emit("acceleration", 0)
}

function backwardClicked() {
	socket.emit("acceleration", -1)	}

function leftClicked() {
	socket.emit("angle", -1);
}

function centerClicked() {
	socket.emit("angle", 0);
}

function rightClicked() {
	socket.emit("angle", 1);
}