
function initialize() {
	var socket = io.connect("/raspicar/admin");
	socket.on("cars", function(data){
		var array = JSON.parse(data);
		for(var i=0; i<array.length; i++) {
			//createQRDiv(array[i].name+":"+array[i].id+" "+array[i].free);
			createInfoDiv(array[i]);
		}
	});
	

	document.getElementById("simbuttonstop").onclick=simulateRaceEnd;
	document.getElementById("simbuttonstart").onclick=simulateRaceStart;
	socket.on("carconnect", function(data){
		createInfoDiv(data);
	})
	
	socket.on("cardc", function(data){
		//remove div here
	})
	//Create a qr code holding an example number (just for poc);
	function createQRDiv(value) {
		var div = document.createElement("div");
		div.innerHTML=value.toString();
		document.body.appendChild(div);
		
	}

	function createInfoDiv(car) {
		var div = document.createElement("div");
		div.id= car.id;
		div.innerHTML = car.name+" "+car.id+" "+car.free;
		document.body.appendChild(div);
	}

	function simulateRaceEnd() {
		console.log("simulating race end")
		socket.emit("raceendsim", "");
	}

	function simulateRaceStart() {
		console.log("simulating race start")
		socket.emit("racestartsim","");
	}

	function removeDiv(car) {
		var element = document.getElementById(car.id);
		element.parentNode.removeChild(element);
	}
	
	
	
}



