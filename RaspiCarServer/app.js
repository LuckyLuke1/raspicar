Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

// definiert den Port



var carmanager = require('./carmanager');
app.use(express.static(__dirname + '/user'));
app.use('/admin', express.static(__dirname+ '/../public/admin'));
app.use('/play/[0-9]+', express.static(__dirname+ '/../public/play'));
app.use("/car", express.static(__dirname+"/../public/cartest"));

var ADMIN_STRING ="/raspicar/admin";

var connectedAdmins=[];
var ledController = [];

var clientnsp = io.of("/raspicar/client");
var adminnsp= io.of(ADMIN_STRING);
var carnsp = io.of("/raspicar/car");
var lednsp = io.of("/raspicar/led");

var is_race_going=false;

lednsp.on("connection", function(socket) {
	ledController.push(socket);
	socket.on("raspi/started", function(data){
		is_race_going=true;
	})
	socket.on("disconnect", function(data){
		ledController.remove(socket);
	})
}) 

io.on("connection", function(socket){
	console.log("connection received");
	
	socket.on("raspi/register", function(data){
		var id =carmanager.addCar(data, socket);
		console.log("car with name "+data+" connected and got the id: "+id);
		var newCar = carmanager.getSimpleCarById(id);
		connectedAdmins.forEach(function(value){
			value.emit("carconnect", newCar);
		})
		socket.emit("raspi/info", id);
		
		socket.on("disconnect", function() {
			console.log("car disconnected");
			connectedAdmins.forEach(function(value){
				value.emit("cardc", newCar);
			})
			carmanager.removeCar(socket);
		});

		socket.on("raspi/green", function(data) {
			console.log("car with name "+newCar.name+" sent the message green");
		})
	})
})

carnsp.on("connection", function(socket){
	console.log("connection received");
	
	socket.on("raspi/register", function(data){
		var id =carmanager.addCar(data, socket);
		console.log("car with name "+data+" connected and got the id: "+id);
		var newCar = carmanager.getSimpleCarById(id);
		connectedAdmins.forEach(function(value){
			value.emit("carconnect", newCar);
		})
		socket.emit("raspi/info", id);
		
		socket.on("disconnect", function() {
			console.log("car disconnected");
			connectedAdmins.forEach(function(value){
				value.emit("cardc", newCar);
			})
			carmanager.removeCar(socket);
		});

		socket.on("raspi/green", function(data) {
			console.log("car with name "+newCar.name+" sent the message green");
			if(is_race_going) {
				console.log("stopping race");
				is_race_going=false;
			}
		})
	})
})

clientnsp.on('connection', function(socket){
	
	socket.on("register", function(data) {
		var car = carmanager.findCarWithId(data);
		
		if(car && car.free) {
			car.free=false;
			console.log("some1 tried to register a car. it is: ",car);
			if(!carmanager.areFreeCarsThere()){
				console.log("all cars are taken ready to start the race");
				socket.emit("raspi/rstart","");
			}
			socket.on("angle", function(data) {
				if(is_race_going) {
					console.log("angle "+data)
					if(car.socket)
						car.socket.emit("raspi/angle", data);
					else
						console.log("no socket found to emit angle value")
					}
				});
		
				socket.on("acceleration", function(data) {
					if(is_race_going) {
						console.log("acc "+data)
						if(car.socket){
							car.socket.emit("raspi/acceleration", data);
						}
						else
							console.log("no socket found to emit angle value")
					}
					
			});
		
			socket.on("disconnect", function() {
				if(car) {
					car.free=true;
					if(car.socket) {
						car.socket.emit("raspi/stop", "");
						console.log("client disconnected, sending stop message to car: "+car.name)
					}
				}			
			})
		} else if(car){
			//Car is exists but car is taken
			socket.emit("info", "Das Auto ist leider bereits vergeben")
		} else {
			//Car does not exist
			socket.emit("info", "Das Auto wurde nicht gefunden")
		}
	})
	
});

adminnsp.on("connection", function(socket){
	connectedAdmins.push(socket);
	console.log("admin connected now "+connectedAdmins.length+" admins connected");
	
	var cars = carmanager.getAllCars();
	socket.emit("cars", JSON.stringify(cars));
	socket.on("disconnect", function() {
		console.log("admin disconnected");
		connectedAdmins.remove(socket);

	});

	socket.on("raceendsim", function(data){
		if(is_race_going) {
			console.log("received order to simulate race end");
			raceEnd();
		} else {
			console.log("received order to simulate race end, but there is no race runnning..")
		}
		
	})

	socket.on("racestartsim", function(data){
		if(!is_race_going) {
			console.log("received order to simulate race start");
			raceStart();
		} else {
			console.log("received order to simulate race start, but there is already a race running..");
			
		}
	})

	
});

function raceEnd() {
	is_race_going=false;
	io.sockets.emit("raspi/rstop", "")
	io.sockets.emit("raspi/stop","");
}

function raceStart() {
	is_race_going=true;
	io.sockets.emit("raspi/rstart")
}
console.log("finished server startup");

