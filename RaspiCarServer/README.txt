How to connect to the server using a raceberry:
Connect to the server on port 3000, using the standard namespace or if possible the namespace "raspicar/car"
Now register the car. If you are on the standard namespace use socket.emit("raspi/register", "NAMEOFTHECAR"), on the carnamespace use socket.emit("register", "NAMEOFTHECAR").
After that the car will receive the messages "raspi/angle" and "raspi/acceleration" with the wanted data.

To connect via client:
You will need the id of the car. This can be viewed either on localhost:3000/admin or on the console after a car registered.
After that you can either go to localhost:3000/play/[id] or go to localhost:3000 , enter the id into the textfield and hit enter.
