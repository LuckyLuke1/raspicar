System.register(["angular2/core", "../user/connection.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, connection_service_1;
    var PlayComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (connection_service_1_1) {
                connection_service_1 = connection_service_1_1;
            }],
        execute: function() {
            PlayComponent = (function () {
                function PlayComponent(connectionService) {
                    this.connectionService = connectionService;
                    console.log(this.connectionService, " cons");
                }
                PlayComponent.prototype.ngAfterViewInit = function () {
                    window.addEventListener("deviceorientation", function (event) {
                    });
                };
                PlayComponent.prototype.ngOnDestroy = function () {
                };
                PlayComponent.prototype.handleOrientation = function (event) {
                    console.log(event);
                    console.log(this);
                    console.log(this.connectionService + " inside event");
                    var angle = event.beta;
                    if (angle > -45 && angle < 45) {
                        connection_service_1.ConnectionService.emit("angle", -angle / 45);
                        PlayComponent.accelerationValues.push(-angle / 45);
                        if (PlayComponent.accelerationValues.length > 15)
                            PlayComponent.accelerationValues.shift();
                    }
                    var acceleration = event.gamma;
                    var abs = Math.abs(acceleration);
                    if (abs > 45 && abs < 90) {
                        var emittedValue = void 0;
                        if (acceleration > 0) {
                            emittedValue = (90 - acceleration) / 45;
                            connection_service_1.ConnectionService.emit("acceleration", (90 - acceleration) / 45);
                        }
                        else {
                            emittedValue = (-90 - acceleration) / 45;
                        }
                        connection_service_1.ConnectionService.emit("acceleration", emittedValue);
                        PlayComponent.accelerationValues.push(emittedValue);
                        alert(PlayComponent.accelerationValues.length);
                        if (PlayComponent.accelerationValues.length > 15)
                            PlayComponent.accelerationValues.shift();
                    }
                };
                PlayComponent.angleValues = [];
                PlayComponent.accelerationValues = [];
                PlayComponent = __decorate([
                    core_1.Component({
                        templateUrl: "app/play/play.component.html",
                        selector: "play",
                        providers: [connection_service_1.ConnectionService]
                    }), 
                    __metadata('design:paramtypes', [connection_service_1.ConnectionService])
                ], PlayComponent);
                return PlayComponent;
            }());
            exports_1("PlayComponent", PlayComponent);
        }
    }
});
//# sourceMappingURL=play.component.js.map