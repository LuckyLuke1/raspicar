import {Component, OnInit, OnDestroy, Host, AfterViewInit} from "angular2/core";

import {ConnectionService} from "../user/connection.service"

@Component({
    templateUrl:"app/play/play.component.html",
    selector:"play",
    providers:[ConnectionService]
})
export class PlayComponent implements OnDestroy, AfterViewInit{



    constructor(public connectionService:ConnectionService) {
        console.log(this.connectionService," cons")
    }

    static angleValues:number[] = [];
    static accelerationValues:number[] = [];
    message:string;
    
    
    ngAfterViewInit() {
        window.addEventListener("deviceorientation", (event) => {
        });
    }

    ngOnDestroy() {
    }

    private handleOrientation(event:any) {

        console.log(event);
        console.log(this);
        console.log(this.connectionService+" inside event");
        let angle:number = event.beta;
        if(angle>-45 && angle<45) {
            ConnectionService.emit("angle", -angle/45);
            PlayComponent.accelerationValues.push(-angle/45);
            if(PlayComponent.accelerationValues.length>15) PlayComponent.accelerationValues.shift();
		}
	
		let acceleration = event.gamma;
		let abs = Math.abs(acceleration);
		if(abs>45 && abs<90) {
            let emittedValue;
			if(acceleration>0) {
                emittedValue = (90-acceleration)/45;
                
                ConnectionService.emit("acceleration", (90-acceleration)/45);
			} else {    
                emittedValue = (-90-acceleration)/45;
            }
            ConnectionService.emit("acceleration",emittedValue);
            PlayComponent.accelerationValues.push(emittedValue);
            alert(PlayComponent.accelerationValues.length);
            if(PlayComponent.accelerationValues.length>15) PlayComponent.accelerationValues.shift();

			
			
		}
    }

}