import {Injectable} from "angular2/core";


@Injectable()
export class AdminConnectionService {

    //Workaround for angular bug making things not referencable when called due to plain js-events
    static socket : any;

    constructor() {
        AdminConnectionService.socket = io.connect("/raspicar/admin");
    }

    register(id:number, callback: (n:number)=>void) {
        AdminConnectionService.socket.emit("register", id);
    }

    addCallback(name:string, callback:(param:any)=>void) {
        AdminConnectionService.socket.on(name, function(data) {
            callback(data);
        });
    }

    removeCallback(name:string, callback:(param:any)=>void) {
        AdminConnectionService.socket.removeListener(name,callback);
    }

    static emit(name:string, data:any) {
        this.socket.emit(name, data);
    }


}
