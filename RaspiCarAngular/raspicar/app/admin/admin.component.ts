import {Component, OnInit, OnDestroy} from "angular2/core"

import {AdminConnectionService} from "./adminconnection.service"
import {CarInfoCompoent} from "../carinfo/carinfo.component"
import {CarInfo} from "../carinfo/carinfo"

@Component({
    selector:"admin",
    templateUrl:"app/admin/admin.component.html",
    providers:[AdminConnectionService],
    directives:[CarInfoCompoent]
})
export class AdminComponent implements OnInit, OnDestroy{


    static cars:CarInfo[]
    constructor(private connectionService:AdminConnectionService) {

    }

    ngOnInit() {
        this.connectionService.addCallback("ids",this.receiveIds );
    }

    ngOnDestroy() {
        this.connectionService.removeCallback("ids", this.receiveIds);
    }

    receiveIds(ids:any) {
        AdminComponent.cars = JSON.parse(ids);
        console.log(AdminComponent.cars[0].free)
        //Push one more number for testing
      //  AdminComponent.cars.push({id:123, name:"uselesscar"});
    }

    cars():CarInfo[] {
        return AdminComponent.cars;
    }


}