import {Component, Input} from "angular2/core";

import {CarInfo} from "./carinfo"

@Component({
    templateUrl:"app/carinfo/carinfo.component.html",
    selector:"carinfo",
    styleUrls:["app/carinfo/carinfo.component.css"]
})
export class CarInfoCompoent {

    @Input()
    car:CarInfo;


}