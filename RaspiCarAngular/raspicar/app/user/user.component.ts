import {Component, OnDestroy, OnInit} from "angular2/core"

import {ConnectionService} from "./connection.service";
import {PlayComponent} from "../play/play.component";

@Component({
    selector:"user",
    templateUrl:"app/user/user.component.html",
    directives:[PlayComponent]
})
export class UserComponent implements OnDestroy, OnInit {
    
    private id:number;

    //Making this non static results in an exception when triing to set the value throught the "receive car" function. Might be an angular bug.
    //Since this is a singleton anyway make it static or not makes no difference
    private static connected:boolean;
    private static carname:string;

    constructor(private connectionService:ConnectionService) {
        
    }

    tryConnect() {
        console.log("triing to connect to: "+this.id);
        ConnectionService.emit("register", this.id);
    }

    ngOnDestroy() {
        this.connectionService.removeCallback("car", this.receiveCar);
    }

    ngOnInit() {
        this.connectionService.addCallback("car", this.receiveCar)
    }

    receiveCar(carname:string) {
        console.log("receiving car")
        UserComponent.connected= true;
        UserComponent.carname=carname;

    }

    connected():boolean {
        return UserComponent.connected;
    }

    carname():string {
        return UserComponent.carname;
    }
}
