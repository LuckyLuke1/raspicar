System.register(["angular2/core", "./connection.service", "../play/play.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, connection_service_1, play_component_1;
    var UserComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (connection_service_1_1) {
                connection_service_1 = connection_service_1_1;
            },
            function (play_component_1_1) {
                play_component_1 = play_component_1_1;
            }],
        execute: function() {
            UserComponent = (function () {
                function UserComponent(connectionService) {
                    this.connectionService = connectionService;
                }
                UserComponent.prototype.tryConnect = function () {
                    console.log("triing to connect to: " + this.id);
                    connection_service_1.ConnectionService.emit("register", this.id);
                };
                UserComponent.prototype.ngOnDestroy = function () {
                    this.connectionService.removeCallback("car", this.receiveCar);
                };
                UserComponent.prototype.ngOnInit = function () {
                    this.connectionService.addCallback("car", this.receiveCar);
                };
                UserComponent.prototype.receiveCar = function (carname) {
                    console.log("receiving car");
                    UserComponent.connected = true;
                    UserComponent.carname = carname;
                };
                UserComponent.prototype.connected = function () {
                    return UserComponent.connected;
                };
                UserComponent.prototype.carname = function () {
                    return UserComponent.carname;
                };
                UserComponent = __decorate([
                    core_1.Component({
                        selector: "user",
                        templateUrl: "app/user/user.component.html",
                        directives: [play_component_1.PlayComponent]
                    }), 
                    __metadata('design:paramtypes', [connection_service_1.ConnectionService])
                ], UserComponent);
                return UserComponent;
            }());
            exports_1("UserComponent", UserComponent);
        }
    }
});
//# sourceMappingURL=user.component.js.map