import {Injectable} from "angular2/core";


@Injectable()
export class ConnectionService {

    //Workaround for angular bug making things not referencable when called due to plain js-events
    static socket : any;

    constructor() {
        ConnectionService.socket = io.connect("/raspicar/client");
    }

    register(id:number, callback: (n:number)=>void) {
        ConnectionService.socket.emit("register", id);
    }

    addCallback(name:string, callback:(param:any)=>void) {
        ConnectionService.socket.on(name, function(data) {
            callback(data);
        });
    }

    removeCallback(name:string, callback:(param:any)=>void) {
        ConnectionService.socket.removeListener(name,callback);
    }

    static emit(name:string, data:any) {
        ConnectionService.socket.emit(name, data);
    }

    


}
