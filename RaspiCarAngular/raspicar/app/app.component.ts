import {Component} from "angular2/core";

import {AdminComponent} from "./admin/admin.component";
import {UserComponent} from "./user/user.component";
import {ConnectionService} from "./user/connection.service"
import {CarComponent} from "./car/car.component"

@Component({
    //template:"<user></user><admin></admin>",
    templateUrl:"app/app.component.html",
    selector:"my-app",
    directives:[AdminComponent, UserComponent, CarComponent],
    providers: [ConnectionService]
})
export class AppComponent {
    displayMode=0;

    adminClicked() {
        this.displayMode=0;
        
    }
    clientClicked() {
        this.displayMode=1;
    }

    carClicked() {
        this.displayMode=2;
    }


}

