System.register(["angular2/core", "./admin/admin.component", "./user/user.component", "./user/connection.service", "./car/car.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, admin_component_1, user_component_1, connection_service_1, car_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (admin_component_1_1) {
                admin_component_1 = admin_component_1_1;
            },
            function (user_component_1_1) {
                user_component_1 = user_component_1_1;
            },
            function (connection_service_1_1) {
                connection_service_1 = connection_service_1_1;
            },
            function (car_component_1_1) {
                car_component_1 = car_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                    this.displayMode = 0;
                }
                AppComponent.prototype.adminClicked = function () {
                    this.displayMode = 0;
                };
                AppComponent.prototype.clientClicked = function () {
                    this.displayMode = 1;
                };
                AppComponent.prototype.carClicked = function () {
                    this.displayMode = 2;
                };
                AppComponent = __decorate([
                    core_1.Component({
                        //template:"<user></user><admin></admin>",
                        templateUrl: "app/app.component.html",
                        selector: "my-app",
                        directives: [admin_component_1.AdminComponent, user_component_1.UserComponent, car_component_1.CarComponent],
                        providers: [connection_service_1.ConnectionService]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map