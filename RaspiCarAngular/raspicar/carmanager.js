

var idRange=1000;

function createManager() {
	function Car(name, socket) {
		this.name=name;
		this.free=true;
		//Keep a reference on the socket so we can publish data later
		this.socket=socket;
		this.id=Math.floor(Math.random()*1000);
	}
	
	var carNames= [];
	var cars = new Array(carNames.length);
	var i;
	for(i=0; i<cars.length; i++) {
		cars[i] = new Car(carNames[i]);
	}
	
	function ensureNoIdDuplicate() {
		for(i=0;i<cars.length;i++) {
			console.log(cars[i]);	
			for(var j=i+1; j<cars.length; j++) {	
				if(cars[i].id==cars[j].id) {
					cars[j].id=Math.floor(Math.random()*1000);
					ensureNoIdDuplicate();
					return;
				}
			}
		}
	}
	ensureNoIdDuplicate();
	
	this.findCarWithId= function(id) {
		for(i=0; i<cars.length;i++) {
			if(cars[i].id == id) {
				return cars[i];
			}
		}
		return undefined;
	}
	
	this.findFreeCar = function() {
		for(i=0; i<cars.length; i++) {
			if(cars[i].free) {
				cars[i].free=false;
				return cars[i];
			}
		}
	}
	
	this.getFreeCars = function() {
		var freecars=[];
		for(i=0; i<cars.length;i++) {
			if(cars[i].free) {
				console.log(cars[i].free+" freee")
				freecars.push({id:cars[i].id, name:cars[i].name, free:cars[i].free});
			}
		}
		
		return freecars;
	}
	
	this.getCars = function() {
		return cars;
	}
	
	this.addCar = function(name, socket) {
		cars.push(new Car(name, socket));
		ensureNoIdDuplicate();
		console.log("registering car with name "+name+" there are now "+cars.length+" cars registered");
	}
	
	this.removeCar = function(socket) {
		for(i=0; i<cars.length;i++) {
			if(cars[i].socket) {
				if(cars[i].socket==socket) {
					cars.remove(cars[i]);
					return;
				}
			}
		}
	}
	
	return this;
	
	
	
	
	
}

module.exports= createManager();
	
	

