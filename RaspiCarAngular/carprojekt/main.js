

module.exports ={
	
	initialize(socket, express, app) {
	
		
		console.log("my module is starting");
		
		app.use("/carprojekt/admin", express.static(__dirname+ '/admin'))
		app.use("/carprojekt", express.static(__dirname+'/public'))
		
		
		var carnsp = socket.of("/carprojekt/client");
		carnsp.on("connection", function(socket){
			console.log("con");
			
			socket.on("message", function(data){
				console.log(data);
				socket.broadcast.emit("message", data);
			})
		})
		
	}
}