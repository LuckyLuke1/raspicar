// definiert den Port
var PORT = 3000;

var http = require('http');

var express = require("express");
var path = require('path');

var app = express();

var httpServer = http.createServer(app);
httpServer.listen(PORT);
var io = require("socket.io")(httpServer);
var nsp = io.of("/raspicar/car");

console.log("server starting..")
/*io.on('connection', function(socket){
	console.log("got connection")
	
	var acc=0.5, angle=0.0;
	
	function emitSomeValues() {
		acc+=(Math.random()/2.0)-0.25;
		angle+=(Math.random())-0.5;
		
		acc=Math.min(1.0,Math.max(acc, 0.0));
		angle=Math.min(1.0,Math.max(angle, -1.0));

		socket.emit("acceleration", acc);
		socket.emit("angle", angle);
		console.log(acc+"  "+angle);
	}
	
	setInterval(emitSomeValues, 500);
	
});*/
nsp.on("connection", function(socket) {
	console.log("got connection")
	
	var acc=0.5, angle=0.0;
	
	function emitSomeValues() {
		acc+=(Math.random()/2.0)-0.25;
		angle+=(Math.random())-0.5;
		
		acc=Math.min(1.0,Math.max(acc, 0.0));
		angle=Math.min(1.0,Math.max(angle, -1.0));

		socket.emit("acceleration", acc);
		socket.emit("angle", angle);
	}
	
	setInterval(emitSomeValues, 1000);
	
})

