Willkommen zu RaspiCar!

RaspiCarServer
    Innerhalb dieses Ordners befindet sich der erste Entwurf des Javascript Codes, welcher nachher in die Struktur der Ausstellung integriert wurde

RaspiCarAngular
    Ein Entwurf das Projekt in Angular2 umzusetzen, was leider aus zeitlichen Gründen nicht geklappt hat

PresentationVersion
    Die Presentationsversion des Projektes. Hier befindet sich der Javascript Code innerhalb der Struktur in der er für die Ausstellung sein muss

RaspiCarPython/car
    Mit "car_start.py" starten Sie das ferngesteurte Auto.
    Mit "calibrate_color.py" werden Arrays ausgegeben als Hilfestellung zur Kalibrierung der Farberkennung.

RaspiCarPython/racetrack
    Mit "trackcontrol.py" wird das LED-Panel der Fahrbahn aktiviert.



Dokumente
    Technische Dokumentation

Dokumente/Design
    Plakat und User Interface Grafiken

Dokumente/Konzept
    Vor Umsetzung recherchierte Informationen und abgegebene Konzept-Dateien

Dokumente/Pläne
    Schaltpläne und Pinbelegungen

Dokumente/Vorlagen+Anleitungen
    Anleitungen und Quellcodes aus dem Internet, teils genutzt als Basis für die Datei "car_start.py"