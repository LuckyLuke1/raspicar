
module.exports = {
	
	
	getLogger: function(){
		return this;
	},
	
	log:function(message){
		console.log("raspicar: "+message);
	},
	
	debug: function(message){
		if(!IS_PRESENTATION_MODE)
			this.log(message);
	}
}
