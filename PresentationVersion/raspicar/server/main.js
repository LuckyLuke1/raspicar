Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

module.exports ={
	
	initialize(io, express, app) {
	
		var MAX_HIGHSCORE_COUNT=3;
		var raceStartTime;
		var oneRoundFinished=false;
		var highScores = [];

		var carmanager = require('./carmanager');
		var logger = require("./raspicarlogger");
		raspicarlogger = logger.getLogger();
		raspicarlogger.debug("Raspicar initializing");
		app.use("/raspicar", express.static(__dirname + '/../public/user'));
		app.use('/raspicar/admin', express.static(__dirname+ '/../public/admin'));
		app.use('/raspicar/play/[0-9]+', express.static(__dirname+ '/../public/play'));
		app.use("/raspicar/car", express.static(__dirname+"/../public/cartest"));
		app.use("/raspicar/sim", express.static(__dirname+"/../public/sim"));

		var ADMIN_STRING ="/raspicar/admin";

		var connectedAdmins=[];
		var ledController = [];
		var playingPersons=[];

		var clientnsp = io.of("/raspicar/client");
		var adminnsp= io.of(ADMIN_STRING);
		var carnsp = io.of("/raspicar/car");
		var lednsp = io.of("/raspicar/led");
		var simnsp = io.of("/raspicar/sim")

		var is_race_going=false;

		simnsp.on("connection",function(socket){
			socket.on("simstop", function(data){
				if(is_race_going) {
					raspicarlogger.debug("received order to simulate race end");
					raceEnd();
				} else {
					raspicarlogger.debug("received order to simulate race end, but there is no race runnning..")
				}
			})

			socket.on("simstart", function(data) {
				if(!is_race_going) {
					raspicarlogger.debug("received order to simulate race start");
					raceStart();
				} else {
					raspicarlogger.debug("received order to simulate race start, but there is already a race running..");
					
				}
			})

			socket.on("clearhigh", function(data){
				highScores=[];
			})
		})
		lednsp.on("connection", function(socket) {
			ledController.push(socket);
			raspicarlogger.debug("Got a connection from ledcontroller")
			socket.on("raspi/started", function(data){
				raspicarlogger.debug("controller sent message that race started")
				is_race_going=true;
			})
			socket.on("disconnect", function(data){
				raspicarlogger.debug("controller disconnected")
				ledController.remove(socket);
			})
		}) 
		carnsp.on("connection", function(socket){
			raspicarlogger.debug("connection received");
			
			socket.on("register", function(data){
				var id =carmanager.addCar(data, socket);
				raspicarlogger.debug("car with name "+data+" connected and got the id: "+id);
				var newCar = carmanager.getSimpleCarById(id);
				connectedAdmins.forEach(function(value){
					value.emit("carconnect", newCar);
				})
				//socket.emit("raspi/info", id);
				
				socket.on("disconnect", function() {
					raspicarlogger.debug("car disconnected");
					var carstring = JSON.stringify(carmanager.getAllCars())
					connectedAdmins.forEach(function(value){
						value.emit("cars", carstring)
					})
					carmanager.removeCar(socket);
				});

				socket.on("green", function(data) {
					raspicarlogger.debug("car with name "+newCar.name+" sent the message green");
					if(is_race_going) {
						if(oneRoundFinished) {
							raspicarlogger.debug("stopping race");
							raceEnd();
							oneRoundFinished=false;
						}
						else {
							oneRoundFinished=true;
							raspicarlogger.debug("one round finished")
						}
						
					}
				})
				
				socket.on("red", function(data) {
					raspicarlogger.debug("Car sent red")
				})
				socket.on("blue", function(data) {
					raspicarlogger.debug("Car sent blue")
				})
			})
		})

		clientnsp.on('connection', function(socket){
			
			socket.on("register", function(data) {
				var car = carmanager.findCarWithId(data);
				
				if(car && car.free) {
					playingPersons.push(socket);
					car.free=false;
					raspicarlogger.debug("some1 tried to register a car. it is: ",car);
					if(!carmanager.areFreeCarsThere()){
						raspicarlogger.debug("all cars are taken ready to start the race");
						raceStart();
					}
					socket.on("angle", function(data) {
						if(is_race_going) {
							//raspicarlogger.debug("angle "+data)
							if(car.socket)
								car.socket.emit("angle", data);
							else
								raspicarlogger.debug("no socket found to emit angle value")
							}
						});
				
						socket.on("acceleration", function(data) {
							if(is_race_going) {
								//raspicarlogger.debug("acc "+data)
								if(car.socket){
									car.socket.emit("acceleration", data);
								}
								else
									raspicarlogger.debug("no socket found to emit angle value")
							}
							
					});

					socket.on("debug", function(data) {
						raspicarlogger.debug(data+" debug");
					})
				
					socket.on("disconnect", function() {
						playingPersons.remove(socket);
						car.free=true;
						if(car.socket) {
							if(is_race_going) {
								raceEnd();
							}
							car.socket.emit("raspi/stop", "");
							raspicarlogger.debug("client disconnected, sending stop message to car: "+car.name)
						}
							
					})
				} else if(car){
					//Car is exists but car is taken
					socket.emit("info", "Das Auto ist leider bereits vergeben")
				} else {
					//Car does not exist
					socket.emit("info", "Das Auto wurde nicht gefunden")
				}
			})
			
		});

		adminnsp.on("connection", function(socket){
			connectedAdmins.push(socket);
			raspicarlogger.debug("admin connected now "+connectedAdmins.length+" admins connected");
			
			var cars = carmanager.getAllCars();
			socket.emit("cars", JSON.stringify(cars));
			socket.emit("highscores", JSON.stringify(highScores))
		
			socket.on("disconnect", function() {
				raspicarlogger.debug("admin disconnected");
				connectedAdmins.remove(socket);

			});

		});

		function raceEnd() {
			var timeTaken = new Date().getTime()-raceStartTime;
			raspicarlogger.debug("Race is over it took: "+timeTaken/1000+" seconds")

			highScores.push(timeTaken);
			highScores.sort(function(a,b){
				return a-b;
			})
			if(highScores.length>MAX_HIGHSCORE_COUNT) {
				highScores.pop();
			}
			is_race_going=false;
			carmanager.messageAllCars("stop", "")
			ledController.forEach(function(value){
				value.emit("raspi/rstop","")
			})
			playingPersons.forEach(function(value){
				value.emit("end","");
			})

			carmanager.shuffle();
			connectedAdmins.forEach(function(value){
				var cars = carmanager.getAllCars();
				value.emit("cars", JSON.stringify(cars))
				value.emit("highscores", JSON.stringify(highScores))
				value.emit("last", timeTaken);
			})
		}

		function raceStart() {
			raceStartTime = new Date().getTime();
			is_race_going=true;
			ledController.forEach(function(value){
				value.emit("raspi/rstart","")
			})
		}
	},
	getAuthors() {
		return ["Patrick Hilgenstock", "Celia Chang", "Benjamin Lukas Bartels", "Maximilian Fischer"]
	}, getTitle() {
		return "RasPiCar"
	}, getInformation() {
		return "Steuerung eines kleinen Modell Autos über Handy"
	}
}