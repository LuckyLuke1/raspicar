

var idRange=1000;

function getRandomId() {
	return Math.floor(Math.random()*idRange)
}

function createManager() {
	function Car(name, socket) {
		this.name=name;
		this.free=true;
		//Keep a reference on the socket so we can publish data later
		this.socket=socket;
		this.id=getRandomId();
		ensureIdUnique();
	}
	
	var carNames= [];
	var cars = new Array(carNames.length);
	var i;
	for(i=0; i<cars.length; i++) {
		cars[i] = new Car(carNames[i]);
	}
	
	this.ensureIdUnique = function() {
		for(var i=1; i<cars.length;i++) {
			var idOk=true;
			for(var j=0;j<i;j++) {
				if(cars[i].id==cars[j].id) {
					cars[i].id=getRandomId();
					j=i;
					i--;
				}
			}
			
			
		}
	}
	
	this.findCarWithId= function(id) {
		for(i=0; i<cars.length;i++) {
			if(cars[i].id == id) {
				return cars[i];
			}
		}
		return undefined;
	}
	
	this.findFreeCar = function() {
		for(i=0; i<cars.length; i++) {
			if(cars[i].free) {
				cars[i].free=false;
				return cars[i];
			}
		}
	}
	
	this.getFreeCarsId = function() {
		var freecars=[];
		for(i=0; i<cars.length;i++) {
			if(cars[i].free) {
				freecars.push(cars[i].id);
			}
		}
		
		return freecars;
	}
	
	this.addCar = function(name, socket) {
		var car = new Car(name,socket);
		cars.push(car);
		return car.id;
	}
	
	this.removeCar = function(socket) {
		for(i=0; i<cars.length;i++) {
			if(cars[i].socket) {
				if(cars[i].socket==socket) {
					cars.remove(cars[i]);
					return;
				}
			}
		}
	}

	this.areFreeCarsThere = function() {
		var free = false;
		cars.forEach(function(value){
			if(value.free) {
				free=true;
			}
		})
		raspicarlogger.debug("returning "+free)
		return free;
	}

	this.getAllCars = function() {
		var simplifiedCars = [];
		cars.forEach(function (value) {
			simplifiedCars.push(simplifyCar(value))
		})
		return simplifiedCars;
	}

	this.getSimpleCarById = function(id) {
		var ret;
		cars.forEach(function(value){
			if(value.id==id) {
				ret = simplifyCar(value);
				
			}
		})
		return ret;
	}

	this.simplifyCar = function(car) {
		return {
			name:car.name,
			id:car.id,
			free:car.free
		}
	}
	
	this.messageAllCars= function(id, message) {
		cars.forEach(function(value){
			if(value.socket)
				value.socket.emit(id,message);
		})
	}

	this.shuffle= function() {	
			for(var i=0; i<cars.length;i++) {
			cars[i].id = getRandomId();
			
		}
		ensureIdUnique();
	}

	

	

	
	return this;
	
	
	
	
	
}

module.exports= createManager();
	
	

