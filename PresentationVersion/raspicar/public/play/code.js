
socket = io.connect("/raspicar/client");


function initialize(){
	canvas = document.getElementById("canvas");
	var BORDER_SIZE = 5;
	var INDICICATOR_SIZE = 10;
	ctx = canvas.getContext("2d");
	image = new Image();
	image.src="dot.png";
	image.onload = function() {
		ctx.fillStyle="red"
		drawDotAt(canvas.clientWidth/2,canvas.clientHeight/2)
	}


	

	window.addEventListener("mousedown", mouseDown);
	window.addEventListener("mouseup", mouseUp);
	canvas.addEventListener("mousemove", onMouseMove, true);
	window.addEventListener("touchmove", onTouch, true);
	window.addEventListener("touchend", onTouchEnd, true);
	
	function onTouchEnd(event){
		var touchList = event.touches;
		if(touchList.length==0) {
			this.drawDotAt(canvas.clientWidth/2, canvas.clientHeight/2)
			socket.emit("angle", 0);
			socket.emit("acceleration", 0);
		}
	}

	function onTouch(event) {
		event.preventDefault();
		var touchList = event.touches;
		for(var i=0; i<touchList.length;i++) {
			var item = touchList.item(i);
			var hitX = parseInt(item.pageX);
			var hitY = parseInt(item.pageY);
			var xPos = hitX-canvas.offsetLeft;
			var yPos = hitY-canvas.offsetTop;
			if(xPos<canvas.width && yPos<canvas.height) {
				
				
				console.log(xPos+"   "+yPos);
				this.drawDotAt(xPos,yPos);
				var angle =  (-screen.width/2+xPos) / (screen.width/2); 
				//var angle = -canvas.width/2+xPos;
				console.log(canvas.width+"  "+xPos);
				console.log(angle);
				if(angle>0.25) {
					angle=0.5
				} else if(angle<-0.25) {
					angle = -0.5;
				} else {
					angle=0;
				}
				
				var acceleration = (screen.height/2-yPos) / (screen.height/2);
				if(angle!=0) {
					if(acceleration>0)
						acceleration=1;
					else
						acceleration=-1;
				} 
						
						
					
				}
				console.log("angle is: "+angle);
				console.log("acceleration is: "+acceleration)
				
				socket.emit("acceleration", acceleration);
				socket.emit("angle", angle);
			}
		}
	}
	
socket.on("connect", function(data) {
	var current = window.location.href;
	var index = current.lastIndexOf("/");
	current= current.substring(0, index);
	index = current.lastIndexOf("/");
	current= current.substring(index+1, current.length);
	socket.emit("register", current );
})
socket.on("car", function(data){
});

socket.on("info", function(data){
	alert(data);
	returnToMain();

})
socket.on("end", function(data){
	alert("The race is over! You will be redirected to the start page")
	returnToMain();
	
})
	
function onRaceEnd() {
	alert("Das Rennen wurde beendet, willst du auf die Startseite weitergeleitet werden?")
}
	
var isMouseDown=false;
function mouseDown() {
	isMouseDown=true;
}
	
function mouseUp() {
	
	isMouseDown=false;
}

	
	
function onMouseMove(event) {
	if(!isMouseDown) return;
	var xPos = event.pageX-canvas.offsetLeft
	var yPos = event.pageY-canvas.offsetTop
	drawDotAt(xPos,yPos);
		
	
	var angle =  (-canvas.width/2+xPos) / (canvas.width/2); 
		
	if(angle>0.25) {
		angle=0.5
	} else if(angle<-0.25) {
		angle = -0.5;
	} else {
		angle=0;
	}
	var acceleration = (canvas.height/2-yPos) / (canvas.height/2);
	//console.log("angle is: "+angle);
	//console.log("acceleration is: "+acceleration)
	socket.emit("acceleration", acceleration);
	socket.emit("angle", angle);
				
}

function drawDotAt(x,y) {

	x =  x/canvas.clientWidth*1000;
	y = y/canvas.clientHeight*1000;
	ctx.clearRect(0,0,canvas.width, canvas.height);
	ctx.drawImage(image,x-image.width/2, y-image.height/2, image.width, image.height)
}


function accelerateClicked() {
	socket.emit("acceleration", 1)
}

function stopClicked() {
	socket.emit("acceleration", 0)
}

function backwardClicked() {
	socket.emit("acceleration", -1)	}

function leftClicked() {
	socket.emit("angle", -1);
}

function centerClicked() {
	socket.emit("angle", 0);
}

function rightClicked() {
	socket.emit("angle", 1);
}

function returnToMain() {
	window.location.href="/raspicar"
}

