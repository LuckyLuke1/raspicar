var cars=[];
var cardiv;
var hsdiv;

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};


function removeAllChilds(domElement) {
	while(domElement.firstChild) {
			domElement.removeChild(domElement.firstChild)
	}
}
function initialize() {
	cardiv = document.getElementById("cars");
	hsdiv = document.getElementById("hs");
	var socket = io.connect("/raspicar/admin");
	socket.on("cars", function(data){
		cars = JSON.parse(data);
		removeAllChilds(cardiv)
		for(var i=0; i<cars.length; i++) {
			createInfoDiv(cars[i]);
		}
	
	});
	

	socket.on("carconnect", function(data){
		createInfoDiv(data);
		cars.push(data);
	})
	
	socket.on("cardc", function(data){
		cars.remove(data);
		removeDiv(data)
	})

	socket.on("shuffle", function(data){
		cars.forEach(function(value){
			removeDiv(value);
		})
		cars=JSON.parse(data);
	})
	socket.on("highscores", function(data){
		removeAllChilds(hsdiv);
		var i=0;
		JSON.parse(data).forEach(function(value, i){
			createHSDiv(i+": "+value/1000);
		})
	})
	var lastdiv = document.getElementById("last");
	socket.on("last", function (data) {
		lastdiv.innerHTML="Das letzte Rennen hat: "+data/1000+" Sekunden gedauert";
	})

	function createInfoDiv(car) {
		var div = document.createElement("div");
		div.id= car.id;
		div.innerHTML = car.name+" "+car.id+" "+car.free;
		cardiv.appendChild(div);
	}

	function createHSDiv(score) {
		var div = document.createElement("div");
		div.id=score;
		div.innerHTML=score;
		hsdiv.appendChild(div)
	}

	function removeDiv(car) {
		console.log("removing div");
		var element = document.getElementById(car.id);
		element.parentNode.removeChild(element);
	}

	
	
	
	
}



