
export interface Module {
    name:string;
    authors:string[];
    info:string;
    link:string;
}