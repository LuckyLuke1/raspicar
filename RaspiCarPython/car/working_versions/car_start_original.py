#!/usr/bin/env python 

# web_control
# version 1.0

import RPi.GPIO as io
io.setmode(io.BCM)
import sys, tty, termios, time
import os
from socketIO_client import SocketIO, BaseNamespace
import threading

import cv2
import time
import numpy
# from PIL import Image
from threading import Timer

import signal


speed = 0
steer = 0
l_speed = 0
r_speed = 0
down_speed = 30
mid_speed = 42
up_speed = 72
maxspeed = mid_speed

camIndex = 0
camera_running = False
color_acceleration_duration = 4.0


motor1a = 27
motor1b = 22
motor2a = 24
motor2b = 25


# Pinbelegung IN1-4

motor1a = 27
motor1b = 22
io.setup(motor1a, io.OUT)
io.setup(motor1b, io.OUT)

motor2a = 24
motor2b = 25
io.setup(motor2a, io.OUT)
io.setup(motor2b, io.OUT)


# Pinbelegung ENA, ENB

motor1a_pwm = 4
motor1b_pwm = 17
io.setup(motor1a_pwm, io.OUT)
io.setup(motor1b_pwm, io.OUT)
motor1_pwm = io.PWM(4,100)
motor1_pwm = io.PWM(17,100)
motor1_pwm.start(0)
motor1_pwm.ChangeDutyCycle(0)

motor2a_pwm = 18
motor2b_pwm = 23
io.setup(motor2a_pwm, io.OUT)
io.setup(motor2b_pwm, io.OUT)
motor2_pwm = io.PWM(18,100)
motor2_pwm = io.PWM(23,100)
motor2_pwm.start(0)
motor2_pwm.ChangeDutyCycle(0)


# Sauberer Programmabbruch

stop_program=False

def handler(signum, frame):
    global stop_program
    stop_program=True





def forward():

    global motor1a
    global motor1b
    global motor2a
    global motor2b

    io.output(motor1a, False)
    io.output(motor1b, True)
    io.output(motor2a, False)
    io.output(motor2b, True)


def reverse():

    global motor1a
    global motor1b
    global motor2a
    global motor2b
    
    io.output(motor1a, True)
    io.output(motor1b, False)
    io.output(motor2a, True)
    io.output(motor2b, False)


def left():

    global motor1a
    global motor1b
    global motor2a
    global motor2b
        
    io.output(motor1a, False)
    io.output(motor1b, True)
    io.output(motor2a, True)
    io.output(motor2b, False)


def right():

    global motor1a
    global motor1b
    global motor2a
    global motor2b
    
    io.output(motor1a, True)
    io.output(motor1b, False)
    io.output(motor2a, False)
    io.output(motor2b, True)

def left_reverse():

    global motor1a
    global motor1b
    global motor2a
    global motor2b
        
    io.output(motor1a, True)
    io.output(motor1b, False)
    io.output(motor2a, False)
    io.output(motor2b, True)


def right_reverse():

    global motor1a
    global motor1b
    global motor2a
    global motor2b
    
    io.output(motor1a, False)
    io.output(motor1b, True)
    io.output(motor2a, True)
    io.output(motor2b, False)


def stop():

    global motor1a
    global motor1b
    global motor2a
    global motor2b
    
    io.output(motor1a, False)
    io.output(motor1b, False)
    io.output(motor2a, False)
    io.output(motor2b, False)
    
    global l_speed
    l_speed = 0
    global r_speed
    r_speed = 0
    global speed
    speed = 0


# Funktion: Motorsteuerung

def motorcontrol():
    
    global speed
    global steer
    global l_speed
    global r_speed
    global maxspeed
    global down_speed
    global mid_speed
    global up_speed
    global motor1_pwm
    global motor2_pwm
    

    # print 'speed vorher:', speed

    if(speed > 0):
        forward()
        # if speed > 1:
        #     print 'warning: speed > 1'
        #     speed = 1.0
        speed = speed if speed <= 1 else 1                            # Sicherung
        l_speed = speed
        r_speed = speed
    elif(speed == 0):
        stop()
        l_speed = 0
        r_speed = 0
    else:
        reverse()
        speed = speed if speed >= -1.0 else -1.0                  # Sicherung
        l_speed = (speed * -1)
        r_speed = (speed * -1)

    if(steer < 0 and speed > 0):
        left()
        steer = steer if steer >= -1.0 else -1.0                   # Sicherung
        # l_speed = l_speed
        
        # r_speed = (r_speed + 3*r_speed*(1+steer))/4
        r_speed = r_speed - (r_speed + 3*r_speed*(1+steer))/4
    elif(steer > 0 and speed > 0):
        right()
        steer = steer if steer <= 1.0 else 1.0                       # Sicherung
        # l_speed = (l_speed + 3*l_speed*(1-steer))/4
        l_speed = l_speed - (l_speed + 3*l_speed*(1-steer))/4
        
        # r_speed = r_speed

    elif(steer < 0 and speed < 0):
        left_reverse()
        steer = steer if steer >= -1.0 else -1.0                       # Sicherung
        # l_speed = (l_speed + 3*l_speed*(1-steer))/4
        r_speed = r_speed - (r_speed + 3*r_speed*(1-steer))/4

    elif(steer > 0 and speed < 0):
        right_reverse()
        steer = steer if steer <= 1.0 else 1.0                       # Sicherung
        # l_speed = (l_speed + 3*l_speed*(1-steer))/4
        l_speed = l_speed - (l_speed + 3*l_speed*(1-steer))/4

    
    l_speed = round(l_speed * maxspeed)
    r_speed = round(r_speed * maxspeed)

    print 'l_speed nachher:', l_speed
    print 'r_speed nachher:', r_speed

    # PWM nur Werte zwischen 0 und 100
    l_speed_pwm = l_speed
    r_speed_pwm = r_speed

    if (l_speed < 0):
        l_speed_pwm = (l_speed * (-1))
    if (r_speed < 0):
        r_speed_pwm = (r_speed * (-1))

    motor1_pwm.ChangeDutyCycle(l_speed_pwm)
    motor2_pwm.ChangeDutyCycle(r_speed_pwm)




def get_color_distance(capture):

    ret, frameBGR = capture.read()

    width = 40
    height = 40

    org_width = numpy.size(frameBGR,1)
    org_height = numpy.size(frameBGR,0)

    x_start = (org_width - width) / 2
    x_end = x_start + width
    y_start = (org_height - height) / 2
    y_end = y_start + height

    croppedBGR = frameBGR[y_start:y_end, x_start:x_end, :]

    #40x40x3

    red1 = numpy.array([80,82,255])
    red1_distance = numpy.sqrt( numpy.sum((croppedBGR-red1)**2, 2) )
    red1_distance = numpy.mean(red1_distance)

    red2 = numpy.array([110,102,205])
    red2_distance = numpy.sqrt( numpy.sum((croppedBGR-red2)**2, 2) )
    red2_distance = numpy.mean(red2_distance)

    green = numpy.array([115,190,0])
    green_distance = numpy.sqrt( numpy.sum((croppedBGR-green)**2, 2) )
    green_distance = numpy.mean(green_distance)

    blue = numpy.array([255,160,165])
    blue_distance = numpy.sqrt( numpy.sum((croppedBGR-blue)**2, 2) )
    blue_distance = numpy.mean(blue_distance)

    # print 'red1_distance: '+red1_distance
    # print 'red2_distance: '+red2_distance
    # print 'green_distance: '+green_distance
    # print 'blue_distance: '+blue_distance

    # Ausgabe

    if (red1_distance < 50):
        # print "ROT"
        return 1
    if (red2_distance < 75):
        # print "ROT"
        return 1

    if (green_distance < 60):
        # print "GRUEN"
        return 2

    if (blue_distance < 45):
        # print "BLAU"
        return 3



# def get_frame_from_cam(capture):

#     ret, frame = capture.read()

#     # rgbframe is numpy array
#     rgbframe = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

#     # file_handle = open('test.npy','r')
#     # rgbframe = numpy.load(file_handle)
#     # file_handle.close()

#     return rgbframe



# Frame croppen und flatten

# def get_flattened_crop_from_frame(frame):

#     width = 40
#     height = 40

#     org_width = numpy.size(frame,1)
#     org_height = numpy.size(frame,0)

#     x_start = (org_width - width) / 2
#     x_end = x_start + width
#     y_start = (org_height - height) / 2
#     y_end = y_start + height

#     cropped = frame[y_start:y_end, x_start:x_end]

#     # Anzeigen des aktuellen flattened_crop: ZUR KALIBRIERUNG
#     # for row in cropped:
#     #     for col in row:
#     #         print col


#     return cropped.reshape((-1,3))



# Farbbereiche definieren (ranges) und auslesen

# def get_key_color_from_colors(colors):

#     key_color_codes = ['red','green','blue']

#     ranges = numpy.array([
#         #[[200,60,75],[245,110,110]],
#         [[140,0,0],[255,150,150]],
#         [[0,180,0],[125,255,125]],
#         [[0,0,180],[155,145,255]]
#     ])

#     results = numpy.zeros((len(ranges)), dtype=numpy.int)

#     for color in colors:
#         for index, key_range in enumerate(ranges):
#             if (color[0]>=key_range[0][0] and color[1]>=key_range[0][1] and color[2]>=key_range[0][2]):
#                 if (color[0]<=key_range[1][0] and color[1]<=key_range[1][1] and color[2]<=key_range[1][2]):
#                     results[index] += 1

#     color_count = numpy.size(colors,0)

#     percents = {
#         'red': float(results[0])/color_count,
#         'green': float(results[1])/color_count,
#         'blue': float(results[2])/color_count
#         }

#     # Farben als 0,1,2 ausgeben

#     if (percents['red'] > 0.9):
#         return 1

#     if (percents['green'] > 0.9):
#         return 2

#     if (percents['blue'] > 0.9):
#         return 3
    
#     # Farben als Text ausgeben

#     # if (percents['red'] > 0.9):
#     #     return 'red'

#     # if (percents['green'] > 0.9):
#     #     return 'green'

#     # if (percents['blue'] > 0.9):
#     #     return 'blue'
    

#     return False



# def get_key_color(capture):

#     frame = get_frame_from_cam(capture)
#     flattened_cropped = get_flattened_crop_from_frame(frame)
#     return get_key_color_from_colors(flattened_cropped)


def reset_color_acceleration():
    
    global maxspeed, mid_speed
    maxspeed = mid_speed
    on_maxspeed_change('reset color acceleration with timer')

    
def process_color():
        
    global maxspeed
    global down_speed
    global mid_speed
    global up_speed
    global camIndex
    global camera_running
    global color_acceleration_duration

    capture = cv2.VideoCapture(camIndex)
    camera_running = True
    last_color = 2
    color_acceleration_timer = Timer(color_acceleration_duration, reset_color_acceleration)


    while (camera_running):

        # versuchen Farbe einzulesen

        # new_color = get_key_color(capture)        # Alte Farberkennungsvariante
        new_color = get_color_distance(capture)

        if (new_color == 1 and last_color != 1):

            maxspeed = down_speed
            on_maxspeed_change('new color detected: red')

            # socketIO.emit('red')

            color_acceleration_timer.cancel()
            color_acceleration_timer = Timer(color_acceleration_duration, reset_color_acceleration)
            color_acceleration_timer.start()

        elif (new_color == 3 and last_color != 3):

            maxspeed = up_speed
            on_maxspeed_change('new color detected blue')

            # socketIO.emit('blue')

            color_acceleration_timer.cancel()
            color_acceleration_timer = Timer(color_acceleration_duration, reset_color_acceleration)
            color_acceleration_timer.start()

        # elif (new_color == 2 and last_color != 2):

        #     socketIO.emit('green')

        elif (new_color == 2):

            socketIO.emit('green')
            time.sleep(2.5)


        last_color = new_color


        time.sleep(0.2)

    capture.release()

    print 'camera capture stopped'

    return 0





# IP-Eingabe fuer WebSockets
# def ip_input():
#     global ip_adress
#     ip_adress = input("Bitte die IP-Adresse angeben (Bsp.:'xxx.xxx.x.xxx'):  ")

# Variablen server_control
# l_speed = 0
# r_speed = 0
# down_speed = 40
# mid_speed = 50
# up_speed = 60
# maxspeed = mid_speed
# speed = 0
# steer = 0



# camIndex = 0
# # capture = cv2.VideoCapture(camIndex)
# hold_activation_time = 5.0
# timer_running = False
# last_color = False


# Funktionen: Vorwaerts, Rueckwaerts, Stop

def on_acceleration(*args):
    global speed 
    data = float(args[0])
    data = 0 if data != data else data    # Sicherung, falls Input String ist
    speed = data
    motorcontrol()
    # print(repr(speed)+ " speed input")

def on_angle(*args):
    global steer
    data = float(args[0])
    data = 0 if data != data else data    # Sicherung, falls Input String ist
    steer = data
    motorcontrol()
    # print(repr(steer)+ " steer input")

def on_maxspeed_change(cause):
    global maxspeed
    motorcontrol()
    # print cause+' (new maxspeed: '+str(maxspeed)+')'

def on_stop(*args):
    global speed, camera_running
    speed = 0
    print 'race stopped!'
    stop()
    # camera_running = False

# def info(*args):
#     global car_id
#     data = args[0]
#     car_id = data


# WebSocket Verbindung + Farberkennung
# ip_input()

# socketIO = SocketIO(ip_adress, 3000)     # localhost ggf. mit entsprechender IP austauschen!
socket = SocketIO('192.168.0.211', 80)

socketIO = socket.define(BaseNamespace, '/raspicar/car')
socketIO.on('angle', on_angle)
socketIO.on('acceleration', on_acceleration)
socketIO.on('stop', on_stop)
# socketIO.on('raspi/info', info)
socketIO.emit('register', 'Black') #, car_id)
camera_thread = threading.Thread(target = process_color)
camera_thread.start()

stop_program=False

signal.signal(signal.SIGINT, handler)


try:
    while(not stop_program):
        socket.wait(seconds=1)
except Exception as e:
    print e, type(e)
    on_stop()

print "Programm-Ende"

on_stop()
# capture.release()

# Clean Up
io.cleanup()

