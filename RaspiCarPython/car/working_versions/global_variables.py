

def init_globals():
    
    import RPi.GPIO as io
    io.setmode(io.BCM)

    

    global down_speed
    global mid_speed
    global up_speed
    global maxspeed
    global l_speed
    global r_speed
    global speed
    global steer

    global motor1a
    global motor1b
    global motor2a
    global motor2b

    global motor2_pwm
    global motor1_pwm

    speed = 0
    steer = 0
    l_speed = 0
    r_speed = 0
    down_speed = 40
    mid_speed = 50
    up_speed = 60
    maxspeed = mid_speed

    motor1a = 27
    motor1b = 22
    motor2a = 24
    motor2b = 25

    # Pinbelegung IN1-4
    motor1a = 27
    motor1b = 22
    io.setup(motor1a, io.OUT)
    io.setup(motor1b, io.OUT)

    motor2a = 24
    motor2b = 25
    io.setup(motor2a, io.OUT)
    io.setup(motor2b, io.OUT)

    # Pinbelegung ENA, ENB
    motor1a_pwm = 4
    motor1b_pwm = 17
    io.setup(motor1a_pwm, io.OUT)
    io.setup(motor1b_pwm, io.OUT)
    motor1_pwm = io.PWM(4,100)
    motor1_pwm = io.PWM(17,100)
    motor1_pwm.start(0)
    motor1_pwm.ChangeDutyCycle(0)

    motor2a_pwm = 18
    motor2b_pwm = 23
    io.setup(motor2a_pwm, io.OUT)
    io.setup(motor2b_pwm, io.OUT)
    motor2_pwm = io.PWM(18,100)
    motor2_pwm = io.PWM(23,100)
    motor2_pwm.start(0)
    motor2_pwm.ChangeDutyCycle(0)