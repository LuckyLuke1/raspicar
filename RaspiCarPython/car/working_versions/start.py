#!/usr/bin/env python 

# web_control
# version 1.0


import sys, tty, termios, time
import os
from socketIO_client import SocketIO
import threading



# Funktionen aus color_detection.py und global_variables.py
from global_variables import init_globals
init_globals()
from color_detection import *
from motor_control import *


# IP-Eingabe fuer WebSockets
# def ip_input():
# 	global ip_adress
# 	ip_adress = input("Bitte die IP-Adresse angeben (Bsp.:'xxx.xxx.x.xxx'):  ")

# Variablen server_control
# l_speed = 0
# r_speed = 0
# down_speed = 40
# mid_speed = 50
# up_speed = 60
# maxspeed = mid_speed
# speed = 0
# steer = 0



# camIndex = 0
# # capture = cv2.VideoCapture(camIndex)
# hold_activation_time = 5.0
# timer_running = False
# last_color = False


# Funktionen: Vorwaerts, Rueckwaerts, Stop

def on_acceleration(*args):
	global speed 
	data = float(args[0])
	data = 0 if data != data else data	# Sicherung, falls Input String ist
	speed = data
	motorcontrol()
	print(repr(speed)+ " speed input")

def on_angle(*args):
	global steer
	data = float(args[0])
	data = 0 if data != data else data	# Sicherung, falls Input String ist
	steer = data
	motorcontrol()
	print(repr(steer)+ " steer input")

def on_stop(*args):
	global speed
	speed = 0
	stop()

def info(*args):
	global car_id
	data = args[0]
	car_id = data


# WebSocket Verbindung + Farberkennung
# ip_input()

# socketIO = SocketIO(ip_adress, 3000)	 # localhost ggf. mit entsprechender IP austauschen!
socketIO = SocketIO('192.168.2.102', 3000)

socketIO.on('raspi/angle', on_angle)
socketIO.on('raspi/acceleration', on_acceleration)
socketIO.on('raspi/stop', on_stop)
# socketIO_client('raspi/info', info)
socketIO.emit('raspi/register', 'bob')
threading.Thread(target = process_color)


try:
	socketIO.wait()
except Exception as e:
	print e, type(e)
	on_stop()

on_stop()
# capture.release()

# Clean Up
io.cleanup()
