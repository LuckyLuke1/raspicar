# web_control
# version 1.0

import RPi.GPIO as io
io.setmode(io.BCM)
import sys, tty, termios, time
import os
from socketIO_client import SocketIO

# IP-Eingabe fuer WebSockets
def ip_input():
	global ip_adress
	ip_adress = input("Bitte die IP-Adresse angeben (Bsp.:'xxx.xxx.x.xxx'):  ")

# Variablen
l_speed = 0
r_speed = 0
maxspeed = 40
speed = 0
steer = 0


# Funktionen: Vorwaerts, Rueckwaerts, Stop
def forward():
    io.output(motor1a, False)
    io.output(motor1b, True)
    io.output(motor2a, False)
    io.output(motor2b, True)

def reverse():
    io.output(motor1a, True)
    io.output(motor1b, False)
    io.output(motor2a, True)
    io.output(motor2b, False)
	
def stop():
    	io.output(motor1a, False)
    	io.output(motor1b, False)
    	io.output(motor2a, False)
    	io.output(motor2b, False)
   	global l_speed
   	l_speed = 0
   	global r_speed
   	r_speed = 0
	global speed
	speed = 0

# Funktion: Motorsteuerung
def motorcontrol():
	global speed
	global steer
	global l_speed
	global r_speed
	global maxspeed

	# print 'speed vorher:', speed

	if(speed > 0):
		forward()
		# if speed > 1:
		# 	print 'warning: speed > 1'
		# 	speed = 1.0
		speed = speed if speed <= 1 else 1 							# Sicherung
		l_speed = speed
		r_speed = speed
	elif(speed == 0):
		stop()
		l_speed = 0
		r_speed = 0
	else:
		reverse()
		speed = speed if speed >= -1.0 else -1.0 					# Sicherung
		l_speed = (speed * -1)
		r_speed = (speed * -1)

	if(steer < 0):
		steer = steer if steer >= -1.0 else -1.0 					# Sicherung
		l_speed = l_speed
		r_speed = (r_speed + 3*r_speed*(1+steer))/4
		# r_speed = r_speed*(-(-1-steer))
	elif(steer > 0):
		steer = steer if steer <= 1.0 else 1.0 						# Sicherung
		l_speed = (l_speed + 3*l_speed*(1-steer))/4
		# l_speed = l_speed*(1-steer)
		r_speed = r_speed
	
	l_speed = round(l_speed * maxspeed)
	r_speed = round(r_speed * maxspeed)

	print 'l_speed nachher:', l_speed
	print 'r_speed nachher:', r_speed

	

	# PWM nur Werte zwischen 0 und 100
	l_speed_pwm = l_speed
	r_speed_pwm = r_speed
	if (l_speed < 0):
		l_speed_pwm = (l_speed * (-1))
	if (r_speed < 0):
		r_speed_pwm = (r_speed * (-1))

	motor1_pwm.ChangeDutyCycle(l_speed_pwm)
	motor2_pwm.ChangeDutyCycle(r_speed_pwm)
	

def on_acceleration(*args):
	global speed 
	data = float(args[0])
	data = 0 if data != data else data	# Sicherung, falls Input String ist
	speed = data
	motorcontrol() 	# als thread? benoetigt abbruchbedingung durch globale variable
					# bei erneutem aufruf
	# print(repr(speed)+ " speed input")

def on_angle(*args):
	global steer
	data = float(args[0])
	data = 0 if data != data else data	# Sicherung, falls Input String ist
	steer = data
	motorcontrol()
	# print(repr(steer)+ " steer input")

def on_stop(*args):
	global speed
	speed = 0
	motorcontrol()


# Pinbelegung IN1-4
motor1a = 27
motor1b = 22
io.setup(motor1a, io.OUT)
io.setup(motor1b, io.OUT)

motor2a = 24
motor2b = 25
io.setup(motor2a, io.OUT)
io.setup(motor2b, io.OUT)

# Pinbelegung ENA, ENB
motor1a_pwm = 4
motor1b_pwm = 17
io.setup(motor1a_pwm, io.OUT)
io.setup(motor1b_pwm, io.OUT)
motor1_pwm = io.PWM(4,100)
motor1_pwm = io.PWM(17,100)
motor1_pwm.start(0)
motor1_pwm.ChangeDutyCycle(0)

motor2a_pwm = 18
motor2b_pwm = 23
io.setup(motor2a_pwm, io.OUT)
io.setup(motor2b_pwm, io.OUT)
motor2_pwm = io.PWM(18,100)
motor2_pwm = io.PWM(23,100)
motor2_pwm.start(0)
motor2_pwm.ChangeDutyCycle(0)

# WebSocket Verbindung
ip_input()

socketIO = SocketIO(ip_adress, 3000) 	# localhost ggf. mit entsprechender IP austauschen!
# socketIO = SocketIO('192.168.0.248', 3000)

socketIO.on('raspi/angle', on_angle)					
socketIO.on('raspi/acceleration', on_acceleration)	
socketIO.on('raspi/stop', on_stop)					
socketIO.emit('raspi/register', 'bob')

# if count_green == 1:
# 	socketIO.emit('raspi/rstart')
# if count_green == 2:
# 	socketIO.emit('raspi/rstop')
#	stop()


try:
 	socketIO.wait() 						# timeout fuer socketio definieren (-> on_stop()) !!!!
except Exception as e:
	print e, type(e)
	on_stop()

#namespace = socketIO.get_namespace('/raspicar/car')
#namespace.on('angle', on_angle)
#namespace.on('acceleration', on_acceleration)
#namespace.on('stop', on_stop)
#try:
#	namespace.wait() 						# timeout fuer socketio definieren (-> on_stop()) !!!!
#except Exception as e:
#	print e, type(e)
#	on_stop()
#	io.cleanup()

on_stop()

# Clean Up
io.cleanup()
