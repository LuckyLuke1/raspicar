import cv2
import time
import numpy
from PIL import Image
from threading import Timer


# Frame von Kamera

def get_frame_from_cam(capture):

    ret, frame = capture.read()

    # rgbframe is numpy array
    rgbframe = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # file_handle = open('test.npy','r')
    # rgbframe = numpy.load(file_handle)
    # file_handle.close()

    return rgbframe


# Frame croppen und flatten

def get_flattened_crop_from_frame(frame):

    width = 40
    height = 40

    org_width = numpy.size(frame,1)
    org_height = numpy.size(frame,0)

    x_start = (org_width - width) / 2
    x_end = x_start + width
    y_start = (org_height - height) / 2
    y_end = y_start + height

    cropped = frame[x_start:x_end, y_start:y_end]

    # Anzeigen des aktuellen flattened_crop: ZUR KALIBRIERUNG
    # for row in cropped:
    #     for col in row:
    #         print col


    return cropped.reshape((-1,3))


# Farbbereiche definieren (ranges) und auslesen

def get_key_color_from_colors(colors):

    key_color_codes = ['red','green','blue']

    ranges = numpy.array([
        #[[200,60,75],[245,110,110]],
        [[140,0,0],[255,150,150]],
        [[0,180,0],[125,255,125]],
        [[0,0,180],[155,145,255]]
    ])

    results = numpy.zeros((len(ranges)), dtype=numpy.int)

    for color in colors:
        for index, key_range in enumerate(ranges):
            if (color[0]>=key_range[0][0] and color[1]>=key_range[0][1] and color[2]>=key_range[0][2]):
                if (color[0]<=key_range[1][0] and color[1]<=key_range[1][1] and color[2]<=key_range[1][2]):
                    results[index] += 1

    color_count = numpy.size(colors,0)

    percents = {
        'red': float(results[0])/color_count,
        'green': float(results[1])/color_count,
        'blue': float(results[2])/color_count
        }

    # Farben als 0,1,2 ausgeben

    if (percents['red'] > 0.9):
        return 1

    if (percents['green'] > 0.9):
        return 2

    if (percents['blue'] > 0.9):
        return 3
    
    # Farben als Text ausgeben

    # if (percents['red'] > 0.9):
    #     return 'red'

    # if (percents['green'] > 0.9):
    #     return 'green'

    # if (percents['blue'] > 0.9):
    #     return 'blue'
    

    return False

def reset_last_color():
    # setzt timer und farbe zurueck
    global last_color, timer_running
    last_color = False
    timer_running = False

def get_key_color(capture):

    frame = get_frame_from_cam(capture)
    flattened_cropped = get_flattened_crop_from_frame(frame)
    return get_key_color_from_colors(flattened_cropped)


hold_activation_time = 4.0
camIndex = 0
capture = cv2.VideoCapture(camIndex)

timer_running = False
running = True
last_color = False

while (running):
    # falls beim letzten Versuch keine Farbe gefunden
    if (last_color == False):
        # versuchen Farbe einzulesen
        last_color = get_key_color(capture)
        print last_color
    # falls neue farbe gefunden und kein timer gefunden
    if (last_color != False and timer_running == False):
        reset_color_timer = Timer(hold_activation_time, reset_last_color)
        reset_color_timer.start()
        timer_running = True
    # if color == 1:
    # print 'Abbremsung!'
    # # time.sleep(hold_activation_time)
    # if color == 2:
    #     print 'Start!'
    #     # time.sleep(hold_activation_time)
    # if color == 3:
    #     print 'Turbo!'
    #     # time.sleep(hold_activation_time)
capture.release()
