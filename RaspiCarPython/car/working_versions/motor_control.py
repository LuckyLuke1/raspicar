import RPi.GPIO as io
io.setmode(io.BCM)
import sys, tty, termios, time
import os

from global_variables import init_globals
init_globals()


def forward():
	global motor1a
	global motor1b
	global motor2a
	global motor2b

	io.output(motor1a, False)
	io.output(motor1b, True)
	io.output(motor2a, False)
	io.output(motor2b, True)

def reverse():
	global motor1a
	global motor1b
	global motor2a
	global motor2b
	
	io.output(motor1a, True)
	io.output(motor1b, False)
	io.output(motor2a, True)
	io.output(motor2b, False)

def left():
	global motor1a
	global motor1b
	global motor2a
	global motor2b
		
	io.output(motor1a, False)
	io.output(motor1b, True)
	io.output(motor2a, True)
	io.output(motor2b, False)

def right():
	global motor1a
	global motor1b
	global motor2a
	global motor2b
	
	io.output(motor1a, True)
	io.output(motor1b, False)
	io.output(motor2a, False)
	io.output(motor2b, True)

def stop():
	global motor1a
	global motor1b
	global motor2a
	global motor2b
	
	io.output(motor1a, False)
	io.output(motor1b, False)
	io.output(motor2a, False)
	io.output(motor2b, False)
	global l_speed
	l_speed = 0
	global r_speed
	r_speed = 0
	global speed
	speed = 0

# Funktion: Motorsteuerung
def motorcontrol():
	global speed
	global steer
	global l_speed
	global r_speed
	global maxspeed
	global down_speed
	global mid_speed
	global up_speed
	global motor1_pwm
	global motor2_pwm
	

	# print 'speed vorher:', speed

	if(speed > 0):
		forward()
		# if speed > 1:
		#     print 'warning: speed > 1'
		#     speed = 1.0
		speed = speed if speed <= 1 else 1		                    # Sicherung
		l_speed = speed
		r_speed = speed
	elif(speed == 0):
		stop()
		l_speed = 0
		r_speed = 0
	else:
		reverse()
		speed = speed if speed >= -1.0 else -1.0		          # Sicherung
		l_speed = (speed * -1)
		r_speed = (speed * -1)

	if(steer < 0):
		# left()
		steer = steer if steer >= -1.0 else -1.0		           # Sicherung
		l_speed = l_speed
		r_speed = (r_speed + 3*r_speed*(1+steer))/4
		# r_speed = r_speed - (r_speed + 3*r_speed*(1+steer))/4
	elif(steer > 0):
		# right()
		steer = steer if steer <= 1.0 else 1.0		               # Sicherung
		l_speed = (l_speed + 3*l_speed*(1-steer))/4
		# l_speed = l_speed - (l_speed + 3*l_speed*(1-steer))/4
		r_speed = r_speed
		print r_speed

    
	l_speed = round(l_speed * maxspeed)
	r_speed = round(r_speed * maxspeed)

	print 'l_speed nachher:', l_speed
	print 'r_speed nachher:', r_speed


	# PWM nur Werte zwischen 0 und 100
	l_speed_pwm = l_speed
	r_speed_pwm = r_speed
	if (l_speed < 0):
		l_speed_pwm = (l_speed * (-1))
	if (r_speed < 0):
		r_speed_pwm = (r_speed * (-1))

	motor1_pwm.ChangeDutyCycle(l_speed_pwm)
	motor2_pwm.ChangeDutyCycle(r_speed_pwm)

