import RPi.GPIO as io
io.setmode(io.BCM)
import sys, tty, termios, time
import os
from socketIO_client import SocketIO, BaseNamespace
import threading

import cv2
import time
import numpy
from threading import Timer


camIndex = 0
camera_running = False
color_acceleration_duration = 4.0
capture = cv2.VideoCapture(camIndex)

def get_color_distance(capture):

    ret, frameBGR = capture.read()

    width = 40
    height = 40

    org_width = numpy.size(frameBGR,1)
    org_height = numpy.size(frameBGR,0)

    x_start = (org_width - width) / 2
    x_end = x_start + width
    y_start = (org_height - height) / 2
    y_end = y_start + height

    croppedBGR = frameBGR[y_start:y_end, x_start:x_end, :]

    print croppedBGR
    #40x40x3

    red1 = numpy.array([80,82,255])
    red1_distance = numpy.sqrt( numpy.sum((croppedBGR-red1)**2, 2) )
    red1_distance = numpy.mean(red1_distance)

    red2 = numpy.array([110,102,205])
    red2_distance = numpy.sqrt( numpy.sum((croppedBGR-red2)**2, 2) )
    red2_distance = numpy.mean(red2_distance)

    green = numpy.array([115,190,0])
    green_distance = numpy.sqrt( numpy.sum((croppedBGR-green)**2, 2) )
    green_distance = numpy.mean(green_distance)

    blue = numpy.array([255,160,165])
    blue_distance = numpy.sqrt( numpy.sum((croppedBGR-blue)**2, 2) )
    blue_distance = numpy.mean(blue_distance)

    # print red1_distance
    # print red2_distance
    # print green_distance
    # print blue_distance



    if (red1_distance < 60):
        print 1
    if (red2_distance < 60):
        print 1

    if (green_distance < 70):
        print 2

    if (blue_distance < 60):
        print 3

    time.sleep(0.25)


while True:
    get_color_distance(capture)

