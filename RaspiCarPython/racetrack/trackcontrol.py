# trackcontrol
# version 2.0

# imports
import RPi.GPIO as io
io.setmode(io.BCM)
import time
import random
import sys
import os
from socketIO_client import SocketIO, BaseNamespace
import threading
import signal

stop_programm=False

def handler(signal, frame):
        global stop_programm
        stop_programm=True

# set pins
ledred = 24
ledorange = 23
ledgreen = 18
panel_ledred = 12
panel_ledblue =  26


race_status = False
color_duration_min = 8
color_duration_max = 17

io.setup(ledred, io.OUT)
io.setup(ledorange, io.OUT)
io.setup(ledgreen, io.OUT)
io.setup(panel_ledred, io.OUT)
io.setup(panel_ledblue, io.OUT)
gamerunning = False

# start panel
def on_startrace(*args):

        global ledred
        global ledorange
        global ledgreen
        global panel_ledred
        global panel_ledblue
        
        global race_status

        race_status = True

        #Ampel
        print 'Ready'
        io.output(ledred, True)
        time.sleep(3.0)
        io.output(ledred, False)
        print 'Steady'
        io.output(ledorange, True)
        time.sleep(3.0)
        io.output(ledorange, False)
        print 'Go!'
        io.output(ledgreen, True)

        socketIO.emit('raspi/started')
        
def loop_panels():
        # Panel red blue

        global race_status
        global color_duration_min
        global color_duration_max

        while True:
                if(race_status == True):
                        gamerunning = True
                else:
                        gamerunning = False

                while(gamerunning):
                        io.output(panel_ledred, True)
                        print 'Panel: red'
                        time.sleep(random.randrange(color_duration_min, color_duration_max))
                        io.output(panel_ledred, False)
                        io.output(panel_ledblue, True)
                        print 'Panel: blue'
                        time.sleep(random.randrange(color_duration_min, color_duration_max))
                        io.output(panel_ledblue, False)

# stop race
def on_stoprace(*args):
       
        global ledred
        global ledorange
        global ledgreen
        global panel_ledred
        global panel_ledblue

        global race_status
        global gamerunning

        race_status = False
        gamerunning = False

        io.output(panel_ledred, False)
        io.output(panel_ledblue, False)
        io.output(ledgreen, False)

        print 'Track End'

# WebSocket Verbindung
socket = SocketIO('192.168.0.237', 80)            #localhost ggf. mit entsprechender IP austauschen!

socketIO = socket.define(BaseNamespace, '/raspicar/led')
socketIO.on('raspi/rstart', on_startrace)
socketIO.on('raspi/rstop', on_stoprace)
led_thread = threading.Thread(target = loop_panels)
led_thread.start()

signal.signal(signal.SIGINT, handler)

try:
        while(not stop_programm):
                socket.wait(seconds=1) 
except Exception as e:
        print e, type(e)
        on_stoprace()

print "Programmende"

on_stoprace()

io.cleanup()
