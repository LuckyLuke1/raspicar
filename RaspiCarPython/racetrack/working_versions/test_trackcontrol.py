# trackcontrol
# version 2.0

# imports
import RPi.GPIO as GPIO
import time
import random
import sys
import os
from socketIO_client import SocketIO

# set pins
ledred = 24
ledorange = 23
ledgreen = 18 
led
panel_ledred = 12
panel_ledblue =  26

GPIO.setmode(GPIO.BOARD)
GPIO.setup (ledgreen, ledred, ledblue, GPIO.OUT)
gamefunning = False

# start panel
def start_race():
        GPIO.output (ledred, True)
        time.sleep(3.0)
        GPIO.output (ledred, False)
        GPIO.output (ledorange, True)
        time.sleep(3.0)
        GPIO.output (ledorange, False)
        while gamerunning:
                GPIO.output (ledgreen, True)

# red and blue panel
def panel():
        while gamerunning:
                GPIO.output(panel_ledred, True)
                time.sleep(random.randrange(3, 9))
                GPIO.output(panel_ledred, False)
                GPIO.output(panel_ledblue, True)
                time.sleep(random.randrange(3,9))
                GPIO.output(panel_ledblue, False)

# stop race
def stop_race():

# Funktionen: Befehl vom Server
def on_startrace (*args):
        start_race()

def on_stoprace(*args):
        stop_race()

# WebSocket Verbindung
socketIO = SocketIO(ip_adress, 3000)            #localhost ggf. mit entsprechender IP austauschen!

socketIO.on('raspi/rstart', on_startrace)                                       
socketIO.on('raspi/rstop', on_stoprace)                                        
socketIO.wait()                                 #timeout fuer socketio definieren

except KeyboardInterrupt:
        pass

GPIO.cleanup()
