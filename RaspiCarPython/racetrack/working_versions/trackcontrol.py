# trackcontrol
# version 1.0

import RPi.GPIO as GPIO 
import time
from socketIO_client import SocketIO

# Pinbelegung
GPIO.setmode (GPIO.BOARD)
GPIO.setup (14, 15, 17, 18, 27, GPIO.OUT)
gamerunning = False

# Funktion: Rennen starten
# Panel 1 Gruen bleibt an
# Panel 2 Rot und Blau abwechselnd
# Panel 3 Rot und Blau abwechelnd
def start_race():
	# output Gruen
	GPIO.output (14, True)

	# ampel
	GPIO.output (x1, True)
	time.sleep (5)
	
	global gamerunning = True
	# output Rot
	while gamerunning:
		GPIO.output (15, True)
		time.sleep (5)
		GPIO.output (15, False)
		print "Rote LED leuchtet"

	# output Blau
	while gamerunning:
		GPIO.output (17, True)
		time.sleep (5)
		GPIO.output (17, False)
		print "Blaue LED leuchtet"

# Funktion: Rennen stoppen
def stop_race():
	global gamerunning = False
    GPIO.output (14, False)
    GPIO.output (15, False)
    GPIO.output (17, False)
    GPIO.output (18, False)
    GPIO.output (27, False)

# Funktionen: Befehl vom Server
def on_startrace (*args):
	#data = float(args[0]) #Umwandlung der Werte in float
	start_race()

def on_stoprace(*args):
	#data = float(args[0]) #Umwandlung der Werte in float
	stop_race()

start_race()



# WebSocket Verbindung
#socketIO = SocketIO(ip_adress, 3000) 	#localhost ggf. mit entsprechender IP austauschen!

#socketIO.on('___', on_startrace)					
#socketIO.on('___', on_stoprace)					
#socketIO.emit('___', '___')
#socketIO.wait()					#timeout fuer socketio definieren

# Clean Up
io.cleanup()
